+++
type = "page"
+++

![WhatTheCTF](/images/wtctf-logo.png)

...and that's a wrap for this year! Thanks to everyone who came along, and took
part - this couldn't have happened without you all!

We're already thinking about next year's event, which hopefully will happen in
the second semester of 2021/2022.

Can't wait that long? We meet every week to hack things and practice for
other CTFs that we take part in; for more information check out
<https://afnom.net>.

If you've got questions, you can email us at <ctf@afnom.net>.

