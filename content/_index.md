+++
type = "page"
+++

![WhatTheCTF](/images/wtctf-logo.png)

It's that time of year again... time for a CTF!

This semester, AFNOM brings you WhatTheCTF 2021, with more fun than ever
before! Through all the challenges, you'll help protect **Ottergystan**, a
nation renowned throughout the world for it's skilled hackers...

Despite the slightly different theming, we'll have just as just as many
challenges as before. We've got websites, pwnables, crypto, networks, and
anything else we manage to think up. See our [Introduction to CTFs](/intro) if
you're still unsure!

We'll be running the event over Friday 9th to Saturday 10th of April, so
make sure to mark the date now! You can register your interest for more
information [here](/signup), so you can get updates as soon as we have them!

