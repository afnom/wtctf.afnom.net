+++
type = "page"
+++

# Introduction to CTFs

If you've never done a Capture the Flag competition before, then you might be
interesting in knowing what you're getting yourself into :D

A CTF is a team-based cyber-security competition taking place over a few days,
with the team that has the most points winning! To earn points, you collect
flags by solving challenges, that require hacking into a piece of software, or
finding some hidden information, or coding a solution script.

A flag is super recognizable - for WhatTheCTF, all our flags look like
`AFNOM{...}` with the dots replaced with a string unique to each challenge.
You'll be able to submit flags on the site where the CTF is hosted to get
points, and see a live scoreboard.

The challenges will mostly cover the following categories:
- **Web** (hack hilariously insecure websites and servers)
- **Crypto** (decrypt ciphers, learn about codes, get annoyed at maths)
- **Reversing** (work out how some program works, and find a hidden secret)
- **Pwn** (buffer overflows, format string vulnerabilities, and all that goodness)
- **Forensics** (everything from analyzing packet traces to extracting hidden files)
- **Misc** (all the fun challenges that didn't fit neatly anywhere else)

Of course, we might have slightly different categories, each year is always
unique! Don't worry if you're not familiar with anything computer security -
our CTFs are always beginner friendly, and you'll be able to pick up the
required skills along the way.

You can see some examples and official writeups from our previous events
[here](https://afnom.net/events/).
